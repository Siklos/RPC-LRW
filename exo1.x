
const MAXNOM = 255;
typedef string type_nom<MAXNOM>;


struct cellule{
	type_nom donnee;
	cellule* cellule_suivante;
};
typedef struct cellule* liste;

struct param{
	type_nom nom_fichier;
	int ecraser;
	liste donnee;
};

program MESSAGE_PROG
{
	version MESSAGE_VERS
	{
		liste ls(string) = 1;
		liste read(string) = 2;
		int write(param) = 3;
	} = 1 ;
} = 0x20000001 ;
