/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "exo1.h"

#include <sys/types.h>
#include <dirent.h>

void print_list(cellule *n) {
    while (n != NULL) {
        printf("Donnees : %s\n", n->donnee);
        n = n->cellule_suivante;
    }
}



void auth_verification(struct svc_req *rqstp){
	struct authunix_parms *aup;
	aup = (struct authunix_parms *)rqstp->rq_clntcred;

	char hostname[1024];
	gethostname(hostname,sizeof(hostname));
	printf("Machine/host name : compare... %s and %s\n",aup->aup_machname,hostname);
	if(strcmp(aup->aup_machname, hostname) == 0) perror("Hostname invalide");

	printf("GID : compare... %d and %d\n",aup->aup_gid,getgid());
	if(aup->aup_gid != getgid()) perror("gid invalide");

	printf("UID : compare... %d and %d\n",aup->aup_uid,getuid());
	if(aup->aup_uid != getuid()) perror("uid invalide");
    switch(rqstp->rq_cred.oa_flavor){
        case AUTH_NULL:
            printf("AUTH_NULL\n");
        case AUTH_UNIX:
            printf("AUTH_UNIX\n");
    }
}

liste*
ls_1_svc(char **argp, struct svc_req *rqstp)
{
	auth_verification(rqstp);
    static liste  result;
	struct dirent *fichier_lu;
	DIR *rep = opendir(*argp);
	cellule* cel_prec = NULL;
	cellule* cel = NULL;
	while((fichier_lu = readdir(rep))){
		cel = (cellule *)malloc(sizeof(cellule*));
		cel->donnee = (char*)malloc(100);
		strcpy(cel->donnee,fichier_lu->d_name);
		cel->cellule_suivante = NULL;
		if(cel_prec == NULL){
			result = cel;
		}else{
			cel_prec->cellule_suivante = cel;
		}
		cel_prec = cel;
	}
	print_list(result);
	return &result;
}

liste *
read_1_svc(char **argp, struct svc_req *rqstp)
{
	static liste result;

	char line[1024] = "";
	FILE *file;
	file = fopen(*argp, "r");
	cellule* cel_prec = NULL;
	cellule* cel = NULL;
	if (file) {
    	while ((fgets(line,sizeof(line),file))){
			printf("Read : %s\n", line);
			cel = (cellule *)malloc(sizeof(cellule*));
			cel->donnee = (char*)malloc(1024);
			strcpy(cel->donnee,line);
			cel->cellule_suivante = NULL;
			if(cel_prec == NULL){
				result = cel;
			}else{
				cel_prec->cellule_suivante = cel;
			}
			cel_prec = cel;

		}
    	fclose(file);
	}
	print_list(result);
	return &result;
}

int *
write_1_svc(param *argp, struct svc_req *rqstp)
{
	printf("Write init...\n");
	static int  result;
	FILE *f;
	printf("Cible : %s\n", argp->nom_fichier);
	if(argp->ecraser == 1){
		f = fopen(argp->nom_fichier, "w+");
	}else{
		f = fopen(argp->nom_fichier, "a+");
	}
	if (f == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	liste n;
	n = argp->donnee;
	print_list(argp->donnee);
	while (n != NULL) {
		printf("Write : %s\n", n->donnee);
		fprintf(f, "%s", n->donnee);
		n = n->cellule_suivante;
	}


	result = 1;
	fclose(f);
	return &result;
}
