/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#ifndef _EXO1_H_RPCGEN
#define _EXO1_H_RPCGEN

#include <rpc/rpc.h>


#ifdef __cplusplus
extern "C" {
#endif

#define MAXNOM 255

typedef char *type_nom;

struct cellule {
	type_nom donnee;
	struct cellule *cellule_suivante;
};
typedef struct cellule cellule;

typedef cellule *liste;

struct param {
	type_nom nom_fichier;
	int ecraser;
	liste donnee;
};
typedef struct param param;

#define MESSAGE_PROG 0x20000001
#define MESSAGE_VERS 1

#if defined(__STDC__) || defined(__cplusplus)
#define ls 1
extern  liste * ls_1(char **, CLIENT *);
extern  liste * ls_1_svc(char **, struct svc_req *);
#define read 2
extern  liste * read_1(char **, CLIENT *);
extern  liste * read_1_svc(char **, struct svc_req *);
#define write 3
extern  int * write_1(param *, CLIENT *);
extern  int * write_1_svc(param *, struct svc_req *);
extern int message_prog_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define ls 1
extern  liste * ls_1();
extern  liste * ls_1_svc();
#define read 2
extern  liste * read_1();
extern  liste * read_1_svc();
#define write 3
extern  int * write_1();
extern  int * write_1_svc();
extern int message_prog_1_freeresult ();
#endif /* K&R C */

/* the xdr functions */

#if defined(__STDC__) || defined(__cplusplus)
extern  bool_t xdr_type_nom (XDR *, type_nom*);
extern  bool_t xdr_cellule (XDR *, cellule*);
extern  bool_t xdr_liste (XDR *, liste*);
extern  bool_t xdr_param (XDR *, param*);

#else /* K&R C */
extern bool_t xdr_type_nom ();
extern bool_t xdr_cellule ();
extern bool_t xdr_liste ();
extern bool_t xdr_param ();

#endif /* K&R C */

#ifdef __cplusplus
}
#endif

#endif /* !_EXO1_H_RPCGEN */
