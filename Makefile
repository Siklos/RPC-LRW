all: serveur client

serveur: exo1_server.c exo1_svc.c exo1_xdr.c exo1.h
	gcc -o serveur exo1_server.c exo1_svc.c exo1_xdr.c

client: exo1_client.c exo1_clnt.c exo1_xdr.c exo1.h
	gcc -o client exo1_client.c exo1_clnt.c exo1_xdr.c
